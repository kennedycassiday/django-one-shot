# Generated by Django 4.1.3 on 2022-12-01 19:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0001_initial"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="Todo",
            new_name="TodoList",
        ),
    ]
